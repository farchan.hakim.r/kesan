var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var SerialPort = require('serialport');
var LocalStorage = require('node-localstorage').LocalStorage;
localStorage = new LocalStorage('./scratch');

const execSync = require('child_process').execSync;

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var path = require('path');
var app = express();

function exists(portName) {
    return new Promise(res => {
        SerialPort.list((err, ports) => {
            res(ports.some(port => port.comName === portName));
        });
    });
}
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(logger('dev'));

//app.use(express.static(path.join(__dirname, 'public')));
app.use('/', express.static('public'));

app.get('/scan', function(req, res) {
    console.log('fd')
    res.sendFile(path.join(__dirname + '/public/scan.html'));
});
app.get('/register', function(req, res) {
    res.sendFile(path.join(__dirname + '/public/registrasi.html'));
});
app.get('/contact', function(req, res) {
    res.sendFile(path.join(__dirname + '/public/contact.html'));
});
app.get('/about', function(req, res) {
    res.sendFile(path.join(__dirname + '/public/about.html'));
});
app.get('/user', function(req, res) {
    res.sendFile(path.join(__dirname + '/public/user.html'));
});
app.get('/admin', function(req, res) {
    res.sendFile(path.join(__dirname + '/public/admin.html'));
});

app.get('/rfidList', function(req, res) {
    res.sendFile(path.join(__dirname + '/public/rfidList.html'));
});

app.get('/rfidRead/:comport', function(req, res) {
    var comport = req.params.comport;
    var port = new SerialPort(comport);


    console.log(req.params.comport);
    port.on("open", function() {
        console.log('open');
        localStorage.setItem('status', 'open');
        port.on('data', function(data) {
            console.log(data.toString('utf8'))
            port.close(function(err) {
                console.log('port closed', err);
                res.status(200).json({
                    data: data.toString('utf8'),
                    status: 'close'
                });
                localStorage.setItem('status', 'close');
            });
        });
    }, function(err) {
        console.log(err)
    });

});

app.get('/comport', function(req, res) {
    const output = execSync('mode', { encoding: 'utf-8' }); // the default is 'buffer'
    var result = output.split("\n");
    res.status(200).send(result);
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});


// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;